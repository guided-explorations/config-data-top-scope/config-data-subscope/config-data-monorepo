
## If This Helps You, Please Star This Project :) <!-- omit in toc -->

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/config-data-top-scope/config-data-subscope/config-data-monorepo)

## Contents <!-- omit in toc -->

<!-- TOC -->
- [Guided Explorations Concept](#guided-explorations-concept)
- [Working Design Pattern](#working-design-pattern)
- [Guided Exploration Exercises Available](#guided-exploration-exercises-available)
- [Overview Information](#overview-information)
- [Demonstrated Design Requirements and Desirements](#demonstrated-design-requirements-and-desirements)
  - [Monorepo Design Pattern](#monorepo-design-pattern)
  - [Managing Complex Configuration Data through Group Layering, Overrides and Per-Environment Scoping](#managing-complex-configuration-data-through-group-layering-overrides-and-per-environment-scoping)
    - [Configuration Data Pattern 1](#configuration-data-pattern-1)
    - [Configuration Data Pattern 2](#configuration-data-pattern-2)
    - [Configuration Data Pattern 3](#configuration-data-pattern-3)
  - [Branch Per Deployment Environment Design Pattern](#branch-per-deployment-environment-design-pattern)
  - [Advanced Use of Includes and Re-Usable Custom Build Extension / Plugin](#advanced-use-of-includes-and-re-usable-custom-build-extension--plugin)
    - [What Would It Look Like Without Includes?](#what-would-it-look-like-without-includes)
    - [Advanced Includes in General](#advanced-includes-in-general)
    - [Advanced Include: Re-Usable Custom Build Extension / Plugin](#advanced-include-re-usable-custom-build-extension--plugin)
  - [Building "Custom Build Extension / Plugins" in GitLab Design Pattern](#building-%22custom-build-extension--plugins%22-in-gitlab-design-pattern)
  - [Make a Custom Extension / Plugin Available as a CI/CD Template Design Pattern](#make-a-custom-extension--plugin-available-as-a-cicd-template-design-pattern)
    - [File Templates](#file-templates)
    - [Project Templates](#project-templates)
    - [Gitlab CI Includes](#gitlab-ci-includes)
- [What and How To Explore:](#what-and-how-to-explore)

<!-- /TOC -->

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)


## Working Design Pattern
As originally built, this design pattern works and can be tested by changing files and seeing builds run.

**Working Design Patterns = Automation of Understanding for Complex Systems**

> **Note:** You can examine closed and in-progress merge requests and issues as well as past pipeline status and logs to preview the flow and controls before attempting your own implementation.

## Guided Exploration Exercises Available
Guided Explorations are designed to be used for training, feature exploration or demostrations.

This repository contains one or more issue templates designed to guide you through:

* setting it up in another location
* observing the major implementation features
* noting what customizations might be made
* making small changes to observe real builds

>  **Note:** To enable Guided Explorations: Setup a duplicate of this repository using these instructions: [.gitlab/issue_templates/01-Setup-Repository.md](.gitlab/issue_templates/01-Setup-Repository.md)  The instructions will eventually have you create a new issue from a template to use as your own Guided Exploration with checklist support for what you have completed.  Each member of a team can use the issue template to track the progress and complete the exploration.

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2019-12-21

* **GitLab Version Released On**: 12.5

* **Last Update**: 2020-07-25

* **GitLab Edition Required**: 

  * For overall solution: [![FC](images/FC.png)](https://about.gitlab.com/features/) 

  * For additional capabilities to **Make a Custom Extension / Plugin Available as a CI/CD Template** ![FC](images/PS.png)

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

* **References and Featured In**:
  
  * [Managing the Complex Configuration Data Management Monster Using GitLab](https://www.youtube.com/watch?v=v4ZOJ96hAck)

## Demonstrated Design Requirements and Desirements

### Monorepo Design Pattern

Leveraging a single repository to build multiple application services or tiers.  Generally used to simplify code collaboration and merge requests for changes that materially affect multiple services at one time.  In GitLab this type of collaboration can also be facilitated *across* repositories via Merge Dependencies - but this does not fully resolve the concerns that monorepos do.

What is demonstrated:

- **.gitlab-ci.yml** "only:changes"![FC](images/FC.png) to limit builds by changes in a given folder so that when one or more services change in the same branch, only those subfolders are built
- **.gitlab-ci.yml** "includes" ![FC](images/FC.png) can be a file in the same repo, another repo or an https url
  - Used to allow per directory includes.
  - Used to enable manage main build templates (gitlab ci custom functions) to have more change controls over them.
- **.gitlab-ci.yml** "extends" ![FC](images/FC.png) parameter for "template jobs" (gitlab ci custom functions) to enable monorepo build to be highly standardized and allow for pipelines configuration to be change-managed in a completely seperate repo if desired.
- 2020-07-25: shell scripting of `source plugin1/.buildvars.sh` - for example of configuration as code for storing configuration varaibles data.
- 2020-07-25: passing environment variables between jobs using a dynamically created shell script (dynamicbuildvars.sh) that is passed forward as an artifact

### Managing Complex Configuration Data through Group Layering, Overrides and Per-Environment Scoping

When deploying the same code base to many environments, configuration data management is paramount.  This includes global defaults and per-subscope overrides of defaults and it includes secrets management.  This repository demonstrates most of the ways GitLab can configure multi-layered data with convergence. This pattern helps demonstrate how GitLab can **compete with the rich configuration data models** of dedicated CD products such as XebiaLabs, Octopus Deply and ArgoCD.  

The configuration data could be **Build-time data** for complex builds and/or **Deployment-time data** for configuring applications in various environments.

- Including supporting **multiple customers** (each have their own deployment repo) **who each have multiple environments** (CI/CD Variable Scopes in each repo) and still being able to leverage upbound groups for configuration data shared across all customers or other sub-scopes of customers.
- **.gitlab-ci.yml** "only:refs"![FC](images/FC.png) to limit CI processing to only environment deployment branches.

#### Configuration Data Pattern 1

This pattern occurs **within a single repo** and would generally be what is used if the same repository is used for building the application and deploying to environments.  A down side is that if the configuration variable permutations get's very large, you end up with one huge CI/CD variable panel.  The pattern shows how to push variables to files when possible to minimize the number of them that need to be in the repo CI/CD Variables.

- **GitLab Feature ![FC](images/FC.png):** variables with environment scopes (WITHOUT Kubernetes)
- **.gitlab-ci.yml** "environment:name: $CI_COMMIT_REF_NAME" ![FC](images/FC.png)to map branch name to variable environment scopes. (GitLab flow branch per "environment")
- **GitLab Feature ![FC](images/FC.png):** secrets in GitLab variables that are not visible to the developers who use them (in CI logs) - requires that variable is stored in CI/CD Variables - not in repo files.  These "Masked" variables have some specific restrictions: https://docs.gitlab.com/ee/ci/variables/#masked-variables
- **GitLab Feature ![FC](images/FC.png):** Only repository "Maintainers" and higher can view and update secrets.
- Complex variable scoping (to meet requirements) on a per-directory, per-gitlab-ci.yml per-variable scope/environment basis
- Offloading variables to files that can be the same for all environment scopes to reduce project complexity and/or enable developer ability to change these variables (security scope)

#### Configuration Data Pattern 2

This pattern takes advantage of GitLab CI/CD Variables being inherited from the upbound group heirarchy that a repository belongs to.  The variables from higher levels can be overridden at lower levels.  This pattern is more applicable when using a repository per-deployment environment.  A common way would be to included only the configuration files for the application in "deployment only" repositories while the application binary artifacts are built by another system entirely.  This also allows optional deployment change control by requiring merge requests for updates to environments.  This also allows different delegated security permissions over the variables at different group levels if there is a need to have varying permissions based on the given variable.

- **GitLab Feature: Group level CI/CD Variables** ![FC](images/FC.png)demonstrate how to create "multi-repo, widely scoped config data defaults" without using environments (which only work at the repo level as of this writing) (TOP SCOPE)
- **GitLab Feature: Group level CI/CD Variables** ![FC](images/FC.png)demonstrate how to create "multi-repo, multi-layered config data with per-layer overrides" without using environments (which only work at the repo level as of this writing) (TOP SCOPE).

#### Configuration Data Pattern 3

Combine Pattern 1 and Pattern 2 for a composite model.  However you model your configuration data, it is important to follow the Einsteinian principle to "Make everything as simple as possible, but not simpler."  Complexity in your data modeling will need to be backed by documentation (hopefully with visuals) and will still increase the probability of mistakes in configuration data.  Be verbose in variable names to indicate their meaning and be sure to document all the elements a given variable will effect - especially if scoped above multiple repositories.

The most likely scenario for this pattern is that you have multiple customers and support multiple environments for each customer.  Customers would each have their own repository (customer scope) and within each repository the environment scopes could be used for a "UAT" and "Production" environment (or more if needed).  Configuration data that spans customers can be specified in parent group levels of the respository.

#### Passing Variables Created In One Job To A Subsequent Job

There patterns are show:
* Using a file stored in the repository (GitOps storage of config data in the repo) - search for ".buildvars.sh" in .build-template.yml
* Using a file generated in the pipeline and stored as an artifact - search for "dynamicbuildvars.sh" in .build-template.yml
* Using artifacts:reports:dotenv.  Variables are read from the file into the pipeline, but the file itself is not stored nor forwarded to the subsequent jobs.  For sensitive information this can be a better option as they are not persisted in artifacts or cache. Documentation - including limitations - is here: https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv.  Additional examples are here: https://docs.gitlab.com/ee/ci/variables/#inherit-environment-variables

### Branch Per Deployment Environment Design Pattern

GitLab supports multiple configurations to target a deployment environment. This repository demonstrates utilizing repository branches to target a deployment environment.  This pattern is useful when:

- Application deployment involves per-environment artifacts, such as configuration files.
- When maximum gating controls are desired over environment updates - by isolating environments by branch, deployments can require a merge request - which is where the maximum gating controls exist within GitLab.
- An alterative is to re-trigger the deploy phase with CI/CD variables to target environments - however this makes the last deployment "records" consist of more ephemeral data and the only available gating control is "Protected Environments".

This pattern can be combined with a repository pattern that also reflects deployment design.  For instance, a repository per SaaS customer and within each repositor, a branch per customer environment (when multiple customer environments are supported, such as "UAT" and "Production")

### Advanced Use of Includes and Re-Usable Custom Build Extension / Plugin

#### What Would It Look Like Without Includes?

The [.gitlab-ci.yml](https://gitlab.com/guided-explorations/config-data-top-scope/config-data-subscope/config-data-monorepo/-/blob/single-gitlab-ci-yml/.gitlab-ci.yml) in the branch "single-gitlab-ci-yml" is a working example of what this monorepo CI would look like if it did not use includes.

#### Advanced Includes in General

Includes play a critical role in making this monorepo pattern managable on a per-sub-directory basis.

This example uses advanced includes to:

* Reuse a bunch of template jobs (similar to CI functions) that are stored in [.build-template.yml](.build-template.yml) - which are non-executable because they start with a dot.
* The plugin folder includes [./plugin1/.gitlab-ci-include.yml](./plugin1/.gitlab-ci-include.yml) and [./plugin2/.gitlab-ci-include.yml](./plugin2/.gitlab-ci-include.yml) then use the "include:" and "extends:" GitLab CI keywords to reuse those template jobs.  "extends:" works ACROSS ALL includes.
* The plugin folder include [./plugin2/.gitlab-ci-include.yml](./plugin2/.gitlab-ci-include.yml) also demonstrates the use of YAML Anchors.  YAML anchors are "per-file" so could be repeated with the same name in each file (say per plugin)
* The base [.gitlab-ci.yml](.gitlab-ci.yml) then includes [.build-template.yml](.build-template.yml), [./plugin1/.gitlab-ci-include.yml](./plugin1/.gitlab-ci-include.yml) and [./plugin2/.gitlab-ci-include.yml](./plugin2/.gitlab-ci-include.yml).

#### Advanced Include: Re-Usable Custom Build Extension / Plugin

The example utilitizes the maven container with Apache FreeMarker to render template configuration files into final config files containing GitLab CI/CD Variables.

- This example uses [Apache FreeMarker](https://freemarker.apache.org) and the [freemarker-cli](https://github.com/sgoeschl/freemarker-cli/blob/master/README.md) project to provide **<u>config file templating capabilities</u>** - similar to Jinja - but Java oriented. It happens to also be the same engine used by the [XebiaLabs](https://xebialabs.com) product.
- When the job is used in a container (here it is using the public maven container) it can be used with any language framework for file templating.  For instance, even if you have a .NET application that needs a bunch of configuration file templating, you can use the job definition in here for your configuration file templating using a free, open source, Apache project standard approach.
- **.gitlab-ci.yml** "artifacts:paths:" ![FC](images/FC.png) to save build artifacts (configuration file in this case) for examination and use in later CI and/or CD jobs or pipelines.

### Building "Custom Build Extension / Plugins" in GitLab Design Pattern

The Configuration File Generation Custom Extension / Plugin also demonstrates how to build a custom extensions/plug-ins and that these can be used regardless of the languages being used by the rest of the CI/CD jobs in the same pipeline.

- The same concepts can be used to build any type of custom build extension you wish around any set of utilities or services running on any custom or publicly maintained container.
- More examples are here: https://gitlab.com/guided-explorations/ci-cd-plugin-extensions

### Make a Custom Extension / Plugin Available as a CI/CD Template Design Pattern

Using GitLabs built-in features for making a custom extension / plugin available as part of new repository templates and new .gitlab-ci.yml files.

#### File Templates

File templates allow a [specific files of the repository can be applied to an existing project](https://docs.gitlab.com/ee/user/admin_area/settings/instance_template_repository.html) (e.g. .gitlab-ci.yml, .Dockerfile, etc)

- For both self-hosted and GitLab.com, you can use the [Group file templates feature](https://gitlab.com/help/user/group/index.md#group-file-templates-premium) ![FC](images/PS.png) can be used.
- If you self-host, you can setup an instance-wide custom file template repository through the [Instance Template Repository](https://docs.gitlab.com/ee/user/admin_area/settings/instance_template_repository.html) feature. ![FC](images/P.png)

#### Project Templates

Project templates used to create an entire repository.  You specify a group that contains all your repository templates and then when users create new repositories, [they can select from the list of templates](https://docs.gitlab.com/ee/gitlab-basics/create-project.html#custom-project-templates-premium).

- For self-hosted or GitLab.com the [Custom Group-Level Project Templates](https://docs.gitlab.com/ee/user/group/custom_project_templates.html#custom-group-level-project-templates-premium) feature. ![FC](images/PS.png)
- If you self-host, you can setup an instance-wide custom project template group through [Custom instance-level project templates](https://docs.gitlab.com/ee/user/admin_area/custom_project_templates.html) feature ![FC](images/P.png).

#### Gitlab CI Includes

In this repository, includes are only shown for files in the same repository, however, the following controls can be applied to make includes into a type of managed template:

* Reference a gitlab-ci.yml include in a separate repo from where it is used and only allow template maintainers to have push and merge capabilities.

* Reference a gitlab-ci.yml include as an https url and ensure only template maintainers can manipulate it.

  **Includes Documentation**: https://docs.gitlab.com/ee/ci/yaml/#include

## What and How To Explore:

* The job logs purposely emit all variables from the entire project so that you can observe what they get set to when they are changed at various levels or how they are different when you merge to branches (deploy to environments) that have different values.
* If you make a copy of the repository and duplicate the setup, you can alter group CI/CD variables:
  * To observe that they flow through to configuration files.
* You can also create fake changes (files plugin1/changeme4subprojectbuild and plugin2/changeme4subprojectbuild) to observe:
  * subprojects only build if they have changes
  * multiple subproject changes in the same branch builds all changed subprojects
  * variable changes as various environments are merged to: "master => sandbox, sandbox => stage, stage => production"
